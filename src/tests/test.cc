#include "controller.h"
#include "gtest/gtest.h"

using namespace s21;

TEST(smart_calc_test, test_model_1) {
  Model model;
  Controller controller(&model);
  string expression = "sqrt(12.5*x^3)/0.09";
  string result = controller.GetResult(expression, 8.0f);
  string expect = "888.8888889";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_2) {
  Model model;
  Controller controller(&model);
  string expression = "(1+(-1)^(x-xmod(1))/2*(x-(x-xmod(1))/2))";
  string result = controller.GetResult(expression, 1.0f);
  string expect = "0.7500000";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_3) {
  Model model;
  Controller controller(&model);
  string expression = "-cos(x^log(5-x))+ln(55/(2+x))";
  string result = controller.GetResult(expression, 3.5f);
  string expect = "1.9842516";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_4) {
  Model model;
  Controller controller(&model);
  string expression = "-(3)*(-x-(7*(-(-(-(-7))))))";
  string result = controller.GetResult(expression, 2.1f);
  string expect = "153.2999997";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_5) {
  Model model;
  Controller controller(&model);
  string expression = "tan(atan(0.5)+asin(sin(sqrt(4)))-acos(0.2))";
  string result = controller.GetResult(expression, 0.0f);
  string expect = "0.2402717";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_6) {
  Model model;
  Controller controller(&model);
  string expression = "(-1+1))";
  string result = controller.GetResult(expression, 0.0f);
  string expect = "Error 13. Missing Open Bracket";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_7) {
  Model model;
  Controller controller(&model);
  string expression = "(--1+1)";
  string result = controller.GetResult(expression, 0.0f);
  string expect = "Error 3. Too many '-'";
  EXPECT_EQ(expect, result);
}
TEST(smart_calc_test, test_model_8) {
  Model model;
  Controller controller(&model);
  string expression = "((-1+1)";
  string result = controller.GetResult(expression, 0.0f);
  string expect = "Error 14. Missing Open Bracket";
  EXPECT_EQ(expect, result);
}
TEST(smart_calc_test, test_model_9) {
  Model model;
  Controller controller(&model);
  string expression = "(1+1)sin(5)";
  string result = controller.GetResult(expression, 0.0f);
  string expect = "Error 5. Lost operator before function";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_10) {
  Model model;
  Controller controller(&model);
  string expression = "sinx";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "0.8939967";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_11) {
  Model model;
  Controller controller(&model);
  string expression = "mod5";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 16. Invalid expression ";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_12) {
  Model model;
  Controller controller(&model);
  string expression = "8mod5 ";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 11. Invalid expression";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_13) {
  Model model;
  Controller controller(&model);
  string expression = "8x+2";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 2. Previous exp is also a number";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_14) {
  Model model;
  Controller controller(&model);
  string expression = "sinsin2";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "0.7890723";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_15) {
  Model model;
  Controller controller(&model);
  string expression = "8(4+2)";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 6. Lost operator before bracket";
  EXPECT_EQ(expect, result);
}
TEST(smart_calc_test, test_model_16) {
  Model model;
  Controller controller(&model);
  string expression = "()";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 7. Missing lexeme before closing brace";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_17) {
  Model model;
  Controller controller(&model);
  string expression = "sin+x";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 8. Incorrect plus sign placement";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_18) {
  Model model;
  Controller controller(&model);
  string expression = "sin(*x)";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 9. Wrong sign position";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_19) {
  Model model;
  Controller controller(&model);
  string expression = "25.2.2";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 10. Unknown lexeme";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_20) {
  Model model;
  Controller controller(&model);
  string expression = "sin2)";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 12. Missing Open Bracket";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_21) {
  Model model;
  Controller controller(&model);
  string expression = "sinmodcos";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 15. Invalid expression";
  EXPECT_EQ(expect, result);
}

TEST(smart_calc_test, test_model_22) {
  Model model;
  Controller controller(&model);
  string expression = "1+sin^cos";
  string result = controller.GetResult(expression, 90.0f);
  string expect = "Error 15. Invalid expression";
}

TEST(smart_calc_test, test_credit_1) {
  Model model;
  Controller controller(&model);
  std::vector<double> result;
  result = controller.CreditResult(20000, 6, 20, true);
  ASSERT_NEAR(result.at(0), 3403.68, 1E-2);
  ASSERT_NEAR(result.at(1), 3403.68, 1E-2);
  ASSERT_NEAR(result.at(2), 3403.68, 1E-2);
  ASSERT_NEAR(result.at(3), 3403.68, 1E-2);
  ASSERT_NEAR(result.at(4), 3403.68, 1E-2);
  ASSERT_NEAR(result.at(5), 3403.68, 1E-2);
  ASSERT_NEAR(result.at(6), 20422.09, 1E-2);
  ASSERT_NEAR(result.at(7), 422.09, 1E-2);
}

TEST(smart_calc_test, test_credit_2) {
  Model model;
  Controller controller(&model);
  std::vector<double> result;
  result = controller.CreditResult(20000, 6, 20, false);
  ASSERT_NEAR(result.at(0), 3666.48, 1E-2);
  ASSERT_NEAR(result.at(1), 3610.95, 1E-2);
  ASSERT_NEAR(result.at(2), 3555.43, 1E-2);
  ASSERT_NEAR(result.at(3), 3499.90, 1E-2);
  ASSERT_NEAR(result.at(4), 3444.38, 1E-2);
  ASSERT_NEAR(result.at(5), 3388.85, 1E-2);
  ASSERT_NEAR(result.at(6), 21166.02, 1E-2);
  ASSERT_NEAR(result.at(7), 1166.02, 1E-2);
}

TEST(smart_calc_test, test_graph_1) {
  Model model;
  Controller controller(&model);

  std::pair<std::vector<double>, std::vector<double>> result;

  result = controller.GetGraphData("1+x", -10, 10);
  ASSERT_FALSE(result.first.empty());
  ASSERT_FALSE(result.second.empty());
}

TEST(smart_calc_test, test_controller_get_expression_1) {
  Model model;
  Controller controller(&model);

  controller.GetResult("1+2*x/5", 0.0f);
  string result = controller.GetExpression();
  ASSERT_EQ(result, "1+2*x/5");
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}