#ifndef SMARTCALCV2_SRC_FRONTEND_GRAPHWINDOW_GRAPH_H_
#define SMARTCALCV2_SRC_FRONTEND_GRAPHWINDOW_GRAPH_H_

#include <QDialog>

#include "controller.h"

namespace Ui {
class graph;
}

class graph : public QDialog {
  Q_OBJECT

public:
  explicit graph(QString expression, s21::Controller *controller,
                 QWidget *parent = nullptr);
  ~graph();

private slots:
  void OnPaintClicked();

private:
  Ui::graph *ui;
  QString expression_;
  s21::Controller *controller_;
};

#endif // SMARTCALCV2_SRC_FRONTEND_GRAPHWINDOW_GRAPH_H_
