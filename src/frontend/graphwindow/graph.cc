#include "graph.h"
#include "ui_graph.h"
#include <QDebug>

graph::graph(QString expression, s21::Controller *controller, QWidget *parent)
    : expression_(expression), controller_(controller), QDialog(parent),
      ui(new Ui::graph) {
  ui->setupUi(this);
  ui->le_expression->setText(expression_);
  connect(ui->btn_paint, &QPushButton::clicked, this,
          [=]() { this->OnPaintClicked(); });
}

graph::~graph() { delete ui; }

void graph::OnPaintClicked() {
  QVector<double> x_data, y_data;
  std::pair<std::vector<double>, std::vector<double>> x_y_data =
      controller_->GetGraphData(ui->le_expression->text().toStdString(),
                                ui->dsb_xmin->value(), ui->dsb_xmax->value());
  x_data = QVector<double>(x_y_data.first.begin(), x_y_data.first.end());
  y_data = QVector<double>(x_y_data.second.begin(), x_y_data.second.end());
  ui->w_plot->addGraph();
  ui->w_plot->graph(0)->setLineStyle(QCPGraph::lsNone);
  ui->w_plot->graph(0)->setScatterStyle(
      QCPScatterStyle(QCPScatterStyle::ssDisc, QColor(160, 70, 150), 2));
  ui->w_plot->xAxis->setLabel("x");
  ui->w_plot->yAxis->setLabel("y");
  ui->w_plot->xAxis->setRange(ui->dsb_xmin->value(), ui->dsb_xmax->value());
  ui->w_plot->yAxis->setRange(ui->dsb_ymin->value(), ui->dsb_ymax->value());
  ui->w_plot->setInteraction(QCP::iRangeZoom, true);
  ui->w_plot->setInteraction(QCP::iRangeDrag, true);
  ui->w_plot->graph(0)->addData(x_data, y_data);
  ui->w_plot->replot();
}
