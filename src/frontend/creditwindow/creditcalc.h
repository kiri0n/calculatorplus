#ifndef SMARTCALCV2_SRC_FRONTEND_CREDITWINDOW_CREDITCALC_H_
#define SMARTCALCV2_SRC_FRONTEND_CREDITWINDOW_CREDITCALC_H_

#include <QDialog>
#include <QStandardItemModel>

#include "controller.h"

namespace Ui {
class creditcalc;
}

class creditcalc : public QDialog {
  Q_OBJECT

public:
  explicit creditcalc(s21::Controller *controller, QWidget *parent = nullptr);
  ~creditcalc();

private slots:
  void OnResultClicked();

private:
  Ui::creditcalc *ui;
  s21::Controller *controller_;
  QStandardItemModel *model_table_;
};

#endif // SMARTCALCV2_SRC_FRONTEND_CREDITWINDOW_CREDITCALC_H_
