#include "creditcalc.h"
#include "ui_creditcalc.h"

creditcalc::creditcalc(s21::Controller *controller, QWidget *parent)
    : controller_(controller), QDialog(parent), ui(new Ui::creditcalc) {
  ui->setupUi(this);
  ui->annu->setChecked(true);
  QStringList labels = QObject::tr("Месяц,Платёж").simplified().split(",");
  model_table_ = new QStandardItemModel;
  model_table_->setColumnCount(2);
  ui->tableView->setModel(model_table_);
  ui->tableView->setColumnWidth(1, 383);
  model_table_->setHorizontalHeaderLabels(labels);
  connect(ui->result_btn, &QPushButton::clicked, this,
          [=]() { this->OnResultClicked(); });
}

creditcalc::~creditcalc() { delete ui; }

void creditcalc::OnResultClicked() {
  model_table_->clear();
  ui->overpayment_l->setText("0");
  ui->total_l->setText("0");
  std::vector<double> result =
      controller_->CreditResult(ui->amount_dsb->value(), ui->period_sb->value(),
                                ui->rate_dsb->value(), ui->annu->isChecked());
  QStandardItem *item = 0;
  for (int i = 0; i < result.size() - 2; ++i) {
    item = new QStandardItem(QString::number(i + 1));
    item->setTextAlignment(Qt::AlignCenter);
    item->setEditable(false);
    model_table_->setItem(i, 0, item);
    item = new QStandardItem(QString::number(result.at(i), 'f', 2));
    item->setTextAlignment(Qt::AlignCenter);
    item->setEditable(false);
    model_table_->setItem(i, 1, item);
  }
  ui->overpayment_l->setText(
      QString::number(result.at(result.size() - 1), 'f', 2));
  ui->total_l->setText(QString::number(result.at(result.size() - 2), 'f', 2));
}
