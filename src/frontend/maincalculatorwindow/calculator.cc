#include "calculator.h"
#include "./ui_calculator.h"
#include <QDebug>

Calculator::Calculator(s21::Controller *controller, QWidget *parent)
    : controller_(controller), QMainWindow(parent), ui(new Ui::Calculator) {
  ui->setupUi(this);
  connect(ui->zero_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("0"); });
  connect(ui->one_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("1"); });
  connect(ui->two_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("2"); });
  connect(ui->three_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("3"); });
  connect(ui->four_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("4"); });
  connect(ui->five_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("5"); });
  connect(ui->six_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("6"); });
  connect(ui->seven_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("7"); });
  connect(ui->eight_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("8"); });
  connect(ui->nine_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("9"); });
  connect(ui->dot_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("."); });
  connect(ui->x_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("x"); });
  connect(ui->cos_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("cos("); });
  connect(ui->acos_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("acos("); });
  connect(ui->sin_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("sin("); });
  connect(ui->asin_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("asin("); });
  connect(ui->tan_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("tan("); });
  connect(ui->atan_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("atan("); });
  connect(ui->ln_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("ln("); });
  connect(ui->log_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("log("); });
  connect(ui->mod_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("mod"); });
  connect(ui->pi_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("3.1415926"); });
  connect(ui->add_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("+"); });
  connect(ui->sub_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("-"); });
  connect(ui->mul_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("*"); });
  connect(ui->div_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("/"); });
  connect(ui->exp_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("^"); });
  connect(ui->sqrt_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("sqrt("); });
  connect(ui->lbkt_btn, &QPushButton::clicked, this,
          [=]() { this->AddText("("); });
  connect(ui->rbkt_btn, &QPushButton::clicked, this,
          [=]() { this->AddText(")"); });

  connect(ui->backspace_btn, &QPushButton::clicked, this,
          [=]() { this->OnBackspaceClicked(); });
  connect(ui->clear_btn, &QPushButton::clicked, this,
          [=]() { this->OnBackspaceClicked(); });

  connect(ui->eq_btn, &QPushButton::clicked, this,
          [=]() { this->OnEqualClicked(); });
  connect(ui->graph_btn, &QPushButton::clicked, this,
          [=]() { this->OnGraphClicked(); });
  connect(ui->credit_btn, &QPushButton::clicked, this,
          [=]() { this->OnCreditClicked(); });
}

Calculator::~Calculator() { delete ui; }

void Calculator::OnBackspaceClicked() {
  QString btn_name = sender()->objectName();
  QRegularExpression re("sin\\($|cos\\($|tan\\($|asin\\($|acos\\($|atan\\($|"
                        "ln\\($|log\\($|mod|sqrt\\($");
  QRegularExpressionMatch match = re.match(ui->lineEdit->text());
  if (btn_name == "backspace_btn") {
    if (ui->lineEdit->text().indexOf("Error") != -1) {
      ui->lineEdit->clear();
    } else if (match.hasMatch()) {
      int length = match.capturedLength(0);
      QString matches = match.captured(0);
      ui->lineEdit->setText(ui->lineEdit->text().remove(
          ui->lineEdit->text().lastIndexOf(matches),
          ui->lineEdit->text().lastIndexOf(match.captured(0)) + length));
    } else {
      ui->lineEdit->backspace();
    }
  } else if (btn_name == "clear_btn") {
    ui->lineEdit->clear();
  }

  TextBoxIsEmpty(sender()->objectName());
}

void Calculator::OnEqualClicked() {
  ui->lineEdit->setText(QString::fromStdString(controller_->GetResult(
      ui->lineEdit->text().toStdString(), ui->x_spinbox->value())));
}

void Calculator::TextBoxIsEmpty(QString btn_name) {
  if (btn_name == "backspace_btn" || btn_name == "clear_btn") {
    if (ui->lineEdit->text() == "") {
      ui->lineEdit->setText("0");
    }
  } else {
    if (ui->lineEdit->text() == "0") {
      ui->lineEdit->clear();
    } else if (ui->lineEdit->text().indexOf("Error") != -1) {
      ui->lineEdit->clear();
    }
  }
}

void Calculator::AddText(QString str) {
  TextBoxIsEmpty(sender()->objectName());
  if (ui->lineEdit->text() == "") {
    if (str == ".")
      str = "0.";
    if (str == ")" || str == "mod")
      str = "0";
  }
  ui->lineEdit->insert(str);
}

void Calculator::OnGraphClicked() {
  if (ui->lineEdit->text() != "0" || ui->lineEdit->text() != "") {
    graph_window_ = new graph(ui->lineEdit->text(), controller_);
    graph_window_->show();
  }
}

void Calculator::OnCreditClicked() {
  calc_window_ = new creditcalc(controller_);
  calc_window_->show();
}
