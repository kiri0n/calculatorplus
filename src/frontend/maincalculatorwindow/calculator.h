#ifndef SMARTCALCV2_SRC_FRONTEND_MAINCALCULATORWINDOW_CALCULATOR_H_
#define SMARTCALCV2_SRC_FRONTEND_MAINCALCULATORWINDOW_CALCULATOR_H_

#include <QMainWindow>
#include <QRegularExpression>

#include "controller.h"
#include "creditcalc.h"
#include "graph.h"
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class Calculator;
}
QT_END_NAMESPACE

class Calculator : public QMainWindow {
  Q_OBJECT

public:
  Calculator(s21::Controller *controller, QWidget *parent = nullptr);
  ~Calculator();
  void Calculate();

private slots:
  void OnBackspaceClicked();
  void OnEqualClicked();
  void AddText(QString str);
  void OnGraphClicked();
  void OnCreditClicked();

private:
  Ui::Calculator *ui;
  s21::Controller *controller_;
  graph *graph_window_;
  creditcalc *calc_window_;

  void TextBoxIsEmpty(QString btn_name);
};
#endif // SMARTCALCV2_SRC_FRONTEND_MAINCALCULATORWINDOW_CALCULATOR_H_
