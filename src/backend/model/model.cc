#include "model.h"

namespace s21 {
std::vector<double> Model::CreditResult(double amount, int period, double rate,
                                        bool type) const {
  std::vector<double> result;
  double amount_tmp = amount;
  int period_tmp = period;
  double overpayment = 0, total = 0;
  if (type == false) {
    double amount_term_tmp = (amount_tmp / period);
    for (int i = 0; i < period_tmp; ++i) {
      double result_tmp =
          amount_term_tmp + (amount_tmp * (rate / 100) * 30.4 / 365);
      amount_tmp -= amount_term_tmp;
      result.push_back(result_tmp);
      total += result_tmp;
    }
    overpayment = (total - amount);
    result.push_back(total);
    result.push_back(overpayment);
  } else {
    double result_tmp =
        amount_tmp *
        ((12 / (100 * rate)) / (1 - pow((1 + (12 / (100 * rate))), -period)));
    overpayment = (result_tmp * period_tmp) - amount_tmp;
    total = overpayment + amount_tmp;
    for (int i = 0; i < period; ++i) {
      result.push_back(result_tmp);
    }
    result.push_back(total);
    result.push_back(overpayment);
  }
  return result;
}

void Model::SetExpression(string expression) {
  ClearExpression();
  str_ = expression;
}

void Model::SetXValue(double x_value) { x_value_ = x_value; }

std::pair<std::vector<double>, std::vector<double>>
Model::GetGraphData(string expression, double x_min, double x_max) {
  ClearExpression();
  SetExpression(expression);
  int n = 10000;
  std::pair<std::vector<double>, std::vector<double>> x_y_pair;
  std::vector<double> x(n + 1), y(n + 1);
  try {
    Parser();
    ReversePolishNotation();
    double step = (x_max - x_min) / n;
    double x_tmp = x_min;
    for (int i = 0; i < n + 1; x_tmp += step, ++i) {
      SetXValue(x_tmp);
      x[i] = x_tmp;
      y[i] = Calculate(input_);
    }
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }
  x_y_pair = std::make_pair(x, y);
  return x_y_pair;
}

string Model::CalcResult(string expression, double x) {
  ClearExpression();
  SetExpression(expression);
  SetXValue(x);
  string result = "";
  try {
    Parser();
    ReversePolishNotation();
    std::stringstream ss;
    ss << std::fixed << std::setprecision(7) << Calculate(input_);
    result = ss.str();
  } catch (std::exception const &err) {
    result = err.what();
  }
  return result;
}

void Model::ClearExpression() {
  str_.clear();
  while (!input_.empty()) {
    input_.pop();
  }
  x_value_ = 0;
}

void Model::Parser() {
  if (str_.length() == 0)
    throw std::invalid_argument("Error 1. No input");
  for (size_t i = 0; i < str_.length(); ++i) {
    if (str_[i] == ',')
      str_[i] = '.';
  }
  size_t current_lenth = 0;
  string previous = "";
  Lexeme prev_lex;
  static const string reg_pattern =
      "sin|cos|tan|asin|acos|atan|log|ln|pi|sqrt|x|mod|\\(|\\)|\\+|-|\\*|/|"
      "\\^|[0-9]*[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?";
  std::regex r(reg_pattern);
  for (std::sregex_iterator it =
           std::sregex_iterator(str_.cbegin(), str_.cend(), r);
       it != std::sregex_iterator(); ++it) {
    std::smatch sm = *it;
    current_lenth += sm.str().length();
    if ((sm.str()[0] >= '0' && sm.str()[0] <= '9') || sm.str()[0] == 'x') {
      if (previous == "0")
        throw std::invalid_argument("Error 2. Previous exp is also a number");
      if (sm.str()[0] == 'x')
        input_.push(Lexeme(sm.str()));
      else
        input_.push(Lexeme(std::stod(sm.str())));
      previous = "0";
      prev_lex.type_ = kNum;
    } else if (sm.str() == "-") {
      if (previous == "-" || previous == "!")
        throw std::invalid_argument("Error 3. Too many '-'");
      if (previous == "0" || previous == ")") {
        input_.push(Lexeme(sm.str()));
        previous = sm.str();
      } else {
        input_.push(Lexeme("!"));
        previous = "!";
      }
      prev_lex.type_ = kOper;
    } else if (sm.str() == "sin" || sm.str() == "cos" || sm.str() == "tan" ||
               sm.str() == "asin" || sm.str() == "acos" || sm.str() == "atan" ||
               sm.str() == "ln" || sm.str() == "log" || sm.str() == "mod" ||
               sm.str() == "sqrt") {
      if ((previous == ")" || previous == "0") && sm.str() != "mod")
        throw std::invalid_argument("Error 5. Lost operator before function");
      input_.push(Lexeme(sm.str()));
      previous = sm.str();
      prev_lex.type_ = kFunc;
    } else if (sm.str() == "(") {
      if (previous == "0" || previous == ")")
        throw std::invalid_argument("Error 6. Lost operator before bracket");
      input_.push(Lexeme(sm.str()));
      previous = sm.str();
      prev_lex.type_ = kOpenBr;
    } else if (sm.str() == ")") {
      if (previous == "" || prev_lex.type_ == kOpenBr ||
          prev_lex.type_ == kFunc)
        throw std::invalid_argument(
            "Error 7. Missing lexeme before closing brace");
      input_.push(Lexeme(sm.str()));
      previous = sm.str();
      prev_lex.type_ = kCloseBr;
    } else if (sm.str() == "+") {
      if (previous != "" && prev_lex.type_ != kOpenBr) {
        if (prev_lex.type_ == kOper || prev_lex.type_ == kFunc)
          throw std::invalid_argument("Error 8. Incorrect plus sign placement");
        input_.push(Lexeme(sm.str()));
        previous = sm.str();
        prev_lex.type_ = kOper;
      }
    } else if (sm.str() == "*" || sm.str() == "/" || sm.str() == "^") {
      if (previous == "" || prev_lex.type_ == kOpenBr ||
          prev_lex.type_ == kFunc || prev_lex.type_ == kOper)
        throw std::invalid_argument("Error 9. Wrong sign position");
      input_.push(Lexeme(sm.str()));
      previous = sm.str();
      prev_lex.type_ = kOper;
    } else {
      throw std::invalid_argument("Error 10. Unknown lexeme");
    }
  }
  if (current_lenth != str_.length()) {
    throw std::invalid_argument("Error 11. Invalid expression");
  }
}

void Model::ReversePolishNotation() {
  std::queue<Lexeme> california;
  std::stack<Lexeme> texas;
  while (!input_.empty()) {
    Lexeme lex = input_.front();
    input_.pop();
    if (lex.type_ == kNum || lex.type_ == kXvalue) {
      california.push(lex);
    } else if (lex.type_ == kFunc || lex.type_ == kOpenBr) {
      texas.push(lex);
    } else if (lex.type_ == kCloseBr) {
      while (!texas.empty() && texas.top().type_ != kOpenBr) {
        Lexeme texas_top = texas.top();
        texas.pop();
        california.push(texas_top);
        if (texas.empty()) {
          throw std::invalid_argument("Error 12. Missing Open Bracket");
        }
      }
      if (!texas.empty() && texas.top().type_ == kOpenBr) {
        texas.pop();
      } else {
        if (texas.empty())
          throw std::invalid_argument("Error 13. Missing Open Bracket");
      }
    } else if (lex.type_ == kOper) {
      while (!texas.empty() && (texas.top().type_ == kFunc ||
                                (texas.top().type_ == kOper &&
                                 texas.top().priority_ >= lex.priority_) ||
                                (texas.top().type_ == kUnary &&
                                 texas.top().priority_ == lex.priority_))) {
        Lexeme texas_top = texas.top();
        texas.pop();
        california.push(texas_top);
      }
      texas.push(lex);
    } else if (lex.type_ == kUnary) {
      texas.push(lex);
    } else {
      while (!texas.empty()) {
        Lexeme texas_top = texas.top();
        texas.pop();
        california.push(texas_top);
      }
    }
  }
  while (!texas.empty()) {
    if (texas.top().type_ == kOpenBr) {
      throw std::invalid_argument("Error 14. Missing Open Bracket");
    }
    Lexeme texas_top = texas.top();
    texas.pop();
    california.push(texas_top);
  }

  input_ = std::move(california);
}

double Model::Calculate(std::queue<Lexeme> input) {
  double result = 0.0;
  std::stack<Lexeme> moscow;
  while (!input.empty()) {
    Lexeme lex = input.front();
    input.pop();
    if (lex.type_ == kXvalue) {
      lex.value_ = x_value_;
      moscow.push(lex);
    } else if (lex.type_ == kNum) {
      moscow.push(lex);
    } else {
      if (lex.type_ == kOpenBr || lex.type_ == kFunc || lex.type_ == kUnary) {
        if (moscow.empty()) {
          throw std::invalid_argument("Error 15. Invalid expression");
        }
        if (moscow.top().type_ == kNum || moscow.top().type_ == kXvalue) {
          double val1 = moscow.top().value_;
          moscow.pop();
          result = lex.CalculateLex(val1);
        }
      } else {
        if (moscow.empty())
          throw std::invalid_argument("Error 23. Invalid expression");
        if (moscow.top().type_ == kNum || moscow.top().type_ == kXvalue) {
          double val1 = moscow.top().value_;
          moscow.pop();
          if (moscow.empty()) {
            throw std::invalid_argument("Error 16. Invalid expression ");
          }
          if (moscow.top().type_ == kNum || moscow.top().type_ == kXvalue) {
            double val2 = moscow.top().value_;
            moscow.pop();
            result = lex.CalculateLex(val1, val2);
          } else {
            throw std::invalid_argument("Error 17. Invalid expression");
          }
        }
      }
      moscow.push(Lexeme(result));
    }
  }
  if (moscow.empty())
    throw std::invalid_argument("Error 18. Invalid expression");
  result = moscow.top().value_;
  moscow.pop();
  return result;
}

double Model::Lexeme::CalculateLex(double const &arg1) {
  double res;
  if (type_ == kOper) {
    throw std::invalid_argument("Error 19. Needs 2 arguments");
  } else {
    if (name_ == "ln") {
      res = log(arg1);
    } else if (name_ == "log") {
      res = log10(arg1);
    } else if (name_ == "sqrt") {
      res = sqrt(arg1);
    } else if (name_ == "tan") {
      res = tan(arg1);
    } else if (name_ == "sin") {
      res = sin(arg1);
    } else if (name_ == "cos") {
      res = cos(arg1);
    } else if (name_ == "atan") {
      res = atan(arg1);
    } else if (name_ == "asin") {
      res = asin(arg1);
    } else if (name_ == "acos") {
      res = acos(arg1);
    } else if (name_ == "!") {
      res = -1.0 * arg1;
    } else {
      throw std::invalid_argument("Error 20. Invalid function name");
    }
  }
  return res;
}

double Model::Lexeme::CalculateLex(double const &arg1, double const &arg2) {
  double res;
  if (type_ != kOper) {
    throw std::invalid_argument("Error 21. Needs only 1 argument");
  } else {
    if (name_ == "+") {
      res = arg2 + arg1;
    } else if (name_ == "-") {
      res = arg2 - arg1;
    } else if (name_ == "*") {
      res = arg2 * arg1;
    } else if (name_ == "/") {
      res = arg2 / arg1;
    } else if (name_ == "^") {
      res = pow(arg2, arg1);
    } else if (name_ == "mod") {
      res = fmod(arg2, arg1);
    } else {
      throw std::invalid_argument("Error 22. Invalid function name");
    }
  }
  return res;
}
} // namespace s21
