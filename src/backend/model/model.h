#ifndef SMARTCALCV2_SRC_BACKEND_CONTROLLER_MODEL_H_
#define SMARTCALCV2_SRC_BACKEND_CONTROLLER_MODEL_H_

#include <cmath>
#include <exception>
#include <iomanip>
#include <iostream>
#include <queue>
#include <regex>
#include <sstream>
#include <stack>
#include <string>
#include <vector>

namespace s21 {
using string = std::string;
class Model {
public:
  class Lexeme {
  public:
    explicit Lexeme(const string &str) : name_(str), value_(0) {
      if (name_ == "x") {
        priority_ = 0;
        type_ = kXvalue;
      } else if (name_ == "(") {
        priority_ = 1;
        type_ = kOpenBr;
      } else if (name_ == ")") {
        priority_ = 1;
        type_ = kCloseBr;
      } else if (name_ == "+" || name_ == "-") {
        priority_ = 2;
        type_ = kOper;
      } else if (name_ == "!") {
        priority_ = 2;
        type_ = kUnary;
      } else if (name_ == "mod" || name_ == "/" || name_ == "*") {
        priority_ = 3;
        type_ = kOper;
      } else if (name_ == "^") {
        priority_ = 4;
        type_ = kOper;
      } else if (name_ == "ln" || name_ == "log" || name_ == "sin" ||
                 name_ == "cos" || name_ == "tan" || name_ == "acos" ||
                 name_ == "asin" || name_ == "atan" || name_ == "sqrt") {
        priority_ = 4;
        type_ = kFunc;
      }
    }

    explicit Lexeme(const double &val)
        : name_(""), value_(val), priority_(0), type_(kNum) {}

    Lexeme() : name_(""), value_(0), priority_(404), type_(kDefault) {}
    Lexeme(Lexeme const &l)
        : name_(l.name_), value_(l.value_), priority_(l.priority_),
          type_(l.type_) {}

    ~Lexeme() { name_.clear(); }
    double CalculateLex(double const &arg1);
    double CalculateLex(double const &arg1, double const &arg2);
    string name_;
    double value_;
    int priority_, type_;
  };

  Model() : str_(""), x_value_(0) {}
  ~Model() { ClearExpression(); }
  string GetExpression() const { return str_; }
  std::pair<std::vector<double>, std::vector<double>>
  GetGraphData(string expression, double x_min, double x_max);
  void SetExpression(string expression);
  void SetXValue(double x_value);
  void ClearExpression();
  void Parser();
  void ReversePolishNotation();
  double Calculate(std::queue<Lexeme> input);

  string CalcResult(string expression, double x);
  std::vector<double> CreditResult(double amount, int period, double rate,
                                   bool type) const;

private:
  typedef enum {
    kNum = 0,
    kXvalue = 1,
    kFunc = 2,
    kUnary = 3,
    kOper = 4,
    kOpenBr = 5,
    kCloseBr = 6,
    kDefault = 7,
  } LexType;
  string str_;
  double x_value_;
  std::queue<Lexeme> input_;
};
} // namespace s21

#endif // SMARTCALCV2_SRC_BACKEND_CONTROLLER_MODEL_H_
