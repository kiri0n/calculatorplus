#include <QApplication>

#include "calculator.h"

int main(int argc, char *argv[]) {
  setenv("LC_NUMERIC", "C", 1);
  QApplication app(argc, argv);
  s21::Model model;
  s21::Controller controller(&model);
  Calculator calculator(&controller);
  calculator.show();
  return app.exec();
}
