#include "controller.h"

namespace s21 {
string Controller::GetResult(const string &str, const double x) {
  return model_->CalcResult(str, x);
}

string Controller::GetExpression() const { return model_->GetExpression(); }

std::pair<std::vector<double>, std::vector<double>>
Controller::GetGraphData(string expression, double x_min, double x_max) {
  return model_->GetGraphData(expression, x_min, x_max);
}

std::vector<double> Controller::CreditResult(double amount, int period,
                                             double rate, bool type) const {
  return model_->CreditResult(amount, period, rate, type);
}
} // namespace s21
