#ifndef SMARTCALCV2_SRC_BACKEND_CONTROLLER_CONTROLLER_H_
#define SMARTCALCV2_SRC_BACKEND_CONTROLLER_CONTROLLER_H_

#include "model.h"

namespace s21 {
class Controller {
public:
  explicit Controller(Model *model) : model_(model){};
  ~Controller() = default;

  string GetResult(const string &str, const double x);
  string GetExpression() const;

  std::vector<double> CreditResult(double amount, int period, double rate,
                                   bool type) const;

  std::pair<std::vector<double>, std::vector<double>>
  GetGraphData(string expression, double x_min, double x_max);

private:
  Model *model_;
};
} // namespace s21

#endif // SMARTCALCV2_SRC_BACKEND_CONTROLLER_CONTROLLER_H_
