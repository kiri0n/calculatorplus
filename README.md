# CalculatorPlus
Implementation of SmartCalculator.

- In this version of the calculator, `Dijkstra's algorithm` is used to translate expressions into `reverse Polish notation`.
- The program is developed in `c++` standard `c++17`
- The `MVC` pattern is used
- The graphical interfase is implemented on the basis of `QT6`
- Full coverage of `unit tests`

### Implemented arithmetic operators:

| Operator name | Infix notation (Classic) | Postfix notation (Reverse Polish notation) |
| -------------- | ------- | ------- |
| Brackets       | (a + b) | a b +   |
| Addition       | a + b   | a b +   |
| Subtraction    | a - b   | a b -   |
| Multiplication | a * b   | a b *   |
| Division       | a / b   | a b \   |
| Power          | a ^ b   | a b ^   |
| Modulus        | a mod b | a b mod |
| Unary plus     | +a      | a+      |
| Unary minus    | -a      | a-      |

      
### Implemented arithmetic functions:

| Function description       | Function |
| -------------------------- | -------- |
| Computes cosine            | cos(x)   |
| Computes sine              | sin(x)   |
| Computes tangent           | tan(x)   |
| Computes arc cosine        | acos(x)  |
| Computes arc sine          | asin(x)  |
| Computes arc tangent       | atan(x)  |
| Computes square root       | sqrt(x)  |
| Computes natural logarithm | ln(x)    |
| Computes common logarithm  | log(x)   |

### Calculator functions:
- Calculation of the entered equations.
- Plotting function graphs.
- Calculation of loan payments.

### Calculator graphical interface
![main](/materials/1.png)
Pic 1.`main window`

\
![graph](/materials/2.png)
Pic 2. `graph window`

\
![credit](/materials/3.png)
Pic 3. `Credit calculator`
